import memoryCache from "./libs/caching";
import { apiPost } from "./utils/api-request";

const auth = async () => {
  const response = await apiPost("/users/login", {
    username: process.env.API_USERNAME || "admin",
    password: process.env.API_PASSWORD || "123",
  });
  const { token } = response;
  memoryCache.set("token", token);
};

export default auth;
