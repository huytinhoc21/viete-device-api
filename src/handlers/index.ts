import { GasStationDetail } from "../types/gas-station";
import { parseConnectFrame } from "./connect";
import { parsePumpStatusFrame, handlePumpStatusData } from "./pump-status";
import { parseStaffShiftFrame, handleStaffShiftData } from "./staff-shift";
import { parseTankStatusFrame, handleTankStatusData } from "./tank-status";
import { parseTransactionFrame, handleTransactionData } from "./transaction";
import sequelize from "../libs/database";
import { logDeviceData } from "../libs/logger";
import { initialGasPump } from "../types/gas-pump";
import { initialGasTank } from "../types/gas-tank";
import memoryCache from "../libs/caching";
import { Transaction } from "../types/transaction";
import { QueryTypes } from "sequelize";

export const checkMissedTransactions = async (
  gasStation: GasStationDetail,
  onReply?: (data: Uint8Array) => Promise<void>
) => {
  const stationName = (await memoryCache.get(
    "stationName_" + gasStation.id
  )) as string;
  const securityKey = (await memoryCache.get(
    "securityKey_" + gasStation.id
  )) as string;

  if (!stationName || !securityKey) {
    return;
  }

  const encoder = new TextEncoder();

  for (const gasPump of gasStation.gas_pumps) {
    const _transactions = await sequelize.query(
      `select * from transactions where gas_pump_id = ? and created_at > ? order by created_at desc limit 900`,
      {
        replacements: [
          gasPump.id,
          new Date(new Date().getTime() - 1000 * 60 * 60 * 24 * 5),
        ],
        type: QueryTypes.SELECT,
      }
    );
    let transactions = (_transactions as Transaction[]).sort((a, b) => {
      const aIndex = a.record_id.substring(a.record_id.length - 4);
      const bIndex = b.record_id.substring(b.record_id.length - 4);
      return parseInt(aIndex) - parseInt(bIndex);
    });
    console.log("gasStation.name", gasStation.name);
    console.log("gasPump.name", gasPump.name);
    console.log(
      `${transactions.length} transactions:`,
      transactions
        .map((t) => parseInt(t.record_id.substring(t.record_id.length - 4)))
        .join(",")
    );
    let lastIndex = -1;
    for (const transaction of transactions) {
      const currIndex = parseInt(
        transaction.record_id.substring(transaction.record_id.length - 4)
      );
      if (
        lastIndex < currIndex - 1 &&
        currIndex - lastIndex < 21 &&
        new Date(transaction.created_at).getTime() >
          new Date().getTime() - 1000 * 60 * 60 * 24 * 5
      ) {
        const transactionReply = `\r\n${stationName}${securityKey}RES${
          gasPump.name
        }${(lastIndex + 1).toString().padStart(4, "0")}${(currIndex - 1)
          .toString()
          .padStart(4, "0")}\r\n`;
        console.log("transactionReply", transactionReply);
        await onReply?.(encoder.encode(transactionReply));
        await new Promise((resolve) =>
          setTimeout(
            () => resolve(""),
            10000 + (currIndex - lastIndex - 1) * 2000
          )
        );
      }
      lastIndex = currIndex;
    }
    if (lastIndex != 899 && 899 - lastIndex < 21) {
      const transactionReply = `\r\n${stationName}${securityKey}RES${
        gasPump.name
      }${(lastIndex + 1).toString().padStart(4, "0")}${(899)
        .toString()
        .padStart(4, "0")}\r\n`;
      console.log("transactionReply", transactionReply);
      await onReply?.(encoder.encode(transactionReply));
      await new Promise((resolve) => setTimeout(() => resolve(""), 10000));
    }
  }
};

const handleFrameData = async (
  buffer: Buffer,
  gasStation: GasStationDetail,
  onReply?: (data: Uint8Array) => Promise<void>
) => {
  let frameData = buffer.toString();
  const arrayBuffer = Array.from(buffer);

  let data: any = {};

  frameData = frameData.trim();
  // Check if frameData starts with a valid frame identifier
  if (!frameData.startsWith("%")) {
    throw new Error("Invalid frame identifier");
  }

  data.frameType = frameData.substring(0, 5);
  data.stationName = frameData.substring(5, 21);
  data.securityKey = frameData.substring(21, 41);

  memoryCache.set("stationName_" + gasStation.id, data.stationName);
  memoryCache.set("securityKey_" + gasStation.id, data.securityKey);

  let result: any = {};
  let dataId = "";

  try {
    // Parse frame data based on frame type
    switch (data.frameType) {
      case "%CONN": // Frame Connect mạng khi mới powerup
        await logDeviceData(arrayBuffer, gasStation);
        parseConnectFrame(frameData, data);
        break;
      case "%RECO": // Frame Connect lại sau khi bị mất kết nối hoặc gửi data không thành công
        await logDeviceData(arrayBuffer, gasStation);
        parseConnectFrame(frameData, data);
        break;
      case "%STAT": // Frame Trạng thái trụ
        parsePumpStatusFrame(arrayBuffer, data);
        // Gắn gas pump tương ứng dựa theo name
        for (let i = 0; i < data.pumpStatuses.length; i++) {
          const gpStatusData = data.pumpStatuses[i];
          const gasPump = gasStation.gas_pumps.find(
            (gp) => gp.name == gpStatusData.name
          );
          data.pumpStatuses[i] = {
            ...(gasPump || initialGasPump),
            status_data: data.pumpStatuses[i],
          };
        }
        await handlePumpStatusData(data);
        break;
      case "%TANK": // Frame Trạng thái trụ
        parseTankStatusFrame(arrayBuffer, data);
        for (let i = 0; i < data.tankStatuses.length; i++) {
          const gtStatusData = data.tankStatuses[i];
          const gasTank = gasStation.gas_tanks.find(
            (gt) => gt.device_name == gtStatusData.tank_name
          );
          data.tankStatuses[i] = {
            ...(gasTank || initialGasTank),
            status_data: data.tankStatuses[i],
          };
        }
        await handleTankStatusData(data);
        break;
      case "%CHIT": // Data chi tiết giao dịch lần bơm bình thường
        dataId = await logDeviceData(arrayBuffer, gasStation);
        parseTransactionFrame(frameData, data);
        result = await handleTransactionData(data, gasStation);
        break;
      case "%GICA": // Data giao ca bình thường
        dataId = await logDeviceData(arrayBuffer, gasStation);
        parseStaffShiftFrame(frameData, data);
        result = await handleStaffShiftData(data, gasStation, onReply);
        break;
      case "%CHWR": // Data chi tiết giao dịch lần bơm được lưu trong bộ nhớ của master
        dataId = await logDeviceData(arrayBuffer, gasStation);
        parseTransactionFrame(frameData, data);
        result = await handleTransactionData(data, gasStation);
        break;
      case "%GIWR": // Data giao ca lưu trong bộ nhớ của master
        dataId = await logDeviceData(arrayBuffer, gasStation);
        parseStaffShiftFrame(frameData, data);
        await handleStaffShiftData(data, gasStation);
        break;
      default:
        throw new Error("Invalid frame type: " + data.frameType);
    }
  } catch (error) {
    if (dataId) {
      await sequelize.query("update device_data set error = $1 where id = $2", {
        bind: [JSON.stringify(error), dataId],
      });
    }
    // throw error;
  }

  // Return parsed frame data
  return { parsedData: data, result };
};

export default handleFrameData;
