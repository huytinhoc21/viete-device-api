import sequelize from "../libs/database";
import { GasPumpDetail } from "../types/gas-pump";
import { getFields, parseTimeDate } from "../utils/parse-helper";

export const handlePumpStatusData = async (data: {
  pumpStatuses: GasPumpDetail[];
}) => {
  for (const gp of data.pumpStatuses) {
    if (gp.id) {
      await sequelize.query(
        "update gas_pumps set status_data = $1 where id = $2",
        {
          bind: [JSON.stringify(gp.status_data), gp.id],
        }
      );
    }
  }
};

// Function to parse Pump Status frame data
export function parsePumpStatusFrame(arrayBuffer: number[], data: any) {
  // Extract frame data

  data.pumpStatuses = [];
  // Parse station status data
  let i = 41;
  for (; i < arrayBuffer.length; i += 25) {
    const status = arrayBuffer[i];
    const pumpStatusArrayBuffer = arrayBuffer.slice(i + 1, i + 25);
    const pumpStatusString = pumpStatusArrayBuffer
      .map((byte) => String.fromCharCode(byte))
      .join("");
    if (pumpStatusString.length < 24) {
      break;
    }
    const fields = getFields(pumpStatusString, [2, 3, 6, 6, 6, 1]);

    const pumpStatus: any = parsePumpStatus(
      fields[0],
      fields[1],
      fields[2],
      fields[3],
      fields[4],
      fields[5]
    );
    pumpStatus.status = status;
    data.pumpStatuses.push(pumpStatus);
  }

  // Parse timestamp
  data.date = parseTimeDate(
    arrayBuffer
      .slice(i, i + 12)
      .map((byte) => String.fromCharCode(byte))
      .join("")
  );

  data.pumpStatuses.forEach((_, index) => {
    data.pumpStatuses[index].station_time = data.date;
    data.pumpStatuses[index].disconnected =
      data.pumpStatuses[index].status == 5;
  });
}

export function parsePumpStatus(
  pump_name,
  gas_type,
  total_cost,
  fuel_volume,
  unit_price,
  coeff
): any {
  const coeffValue = (parseInt(coeff, 16) - 0x30) & 0x0f;
  const coeffLit = coeffValue & 0x03; // Extract lower 2 bits for liters
  const coeffTien = coeffValue >> 2; // Extract upper 2 bits for money

  return {
    name: pump_name,
    gas_type: gas_type,
    total_cost: parseFloat(total_cost.replace(/\,/g, "")) * 10 ** coeffTien, // Convert to decimal with coefficient
    fuel_volume:
      (parseFloat(fuel_volume.replace(/\,/g, "")) / 1000) * 10 ** coeffLit, // Convert to decimal with coefficient
    unit_price: parseFloat(unit_price.replace(/\,/g, "")), // Convert to decimal
  };
}
