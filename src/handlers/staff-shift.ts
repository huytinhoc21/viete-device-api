import { format } from "date-fns";
import sequelize from "../libs/database";
import { GasPumpDetail } from "../types/gas-pump";
import { GasStationDetail } from "../types/gas-station";
import { StaffShift } from "../types/staff-shift";
import { apiPost } from "../utils/api-request";
import { getFields, parseDateTime } from "../utils/parse-helper";
import { resendTransaction } from "./transaction";

export const handleStaffShiftData = async (
  data: {
    frameType: string;
    stationName: string;
    securityKey: string;
    shift: { pump_name: string } & Pick<
      StaffShift,
      | "unit_price"
      | "fuel_volume"
      | "total_revenue"
      | "actual_start_time"
      | "actual_end_time"
      | "starting_volume"
      | "ending_volume"
      | "record_id"
    >;
  },
  gasStation: GasStationDetail,
  onReply?: (data: Uint8Array) => Promise<void>
) => {
  const gasPump = gasStation.gas_pumps.find(
    (gp) => gp.name == data.shift.pump_name
  );
  if (!gasPump) {
    console.log("Gas pump not found!");
    return;
  }

  // Resend missed staff shift
  const shiftReply = await checkAndReplyMissedStaffShift(gasPump, data);
  if (shiftReply) {
    onReply?.(shiftReply);
  }

  await resendTransaction(
    gasPump,
    {
      ...data,
      from: data.shift.actual_start_time,
      to: data.shift.actual_end_time,
    },
    onReply
  );

  const response = await apiPost("/staff_shifts", {
    ...data.shift,
    gas_pump_id: gasPump.id,
  });
  return { reply: shiftReply, response };
};

const checkAndReplyMissedStaffShift = async (
  gasPump: GasPumpDetail, // Replace with actual type if available
  data: {
    stationName: string;
    securityKey: string;
    shift: { actual_start_time: string };
  }
): Promise<Uint8Array | null> => {
  const [lastStaffShifts, _] = await sequelize.query(
    `select * from staff_shifts 
    where deleted_at is null and gas_pump_id = $1 and actual_end_time <= $2 order by actual_end_time desc limit 1`,
    { bind: [gasPump.id, data.shift.actual_start_time] }
  );

  const lastStaffShift = lastStaffShifts[0] as StaffShift;
  if (!lastStaffShift) {
    return null;
  }
  const dateFrom = new Date(lastStaffShift.actual_end_time);

  let shiftReply: Uint8Array | null = null;
  if (
    new Date(data.shift.actual_start_time).getTime() - dateFrom.getTime() >
    5000
  ) {
    // If delta > 5 seconds then reply to request resend data
    return new TextEncoder().encode(
      `\r\n${data.stationName}${data.securityKey}RGC${gasPump.name}${
        format(dateFrom, "HHmmssddMM") + (dateFrom.getFullYear() % 100)
      }\r\n`
    );
  }
  return null;
};

export function parseStaffShiftFrame(frameData: string, data: any) {
  // Extract frame data
  const frameDataString = frameData.substring(41);

  const fields = getFields(
    frameDataString,
    [2, 3, 6, 6, 6, 6, 13, 11, 6, 6, 13, 13, 13]
  );

  const shift: any = {};
  shift.pump_name = fields[0];
  shift.actual_start_time = parseDateTime(fields[2] + fields[3]);
  shift.actual_end_time = parseDateTime(fields[4] + fields[5]);
  shift.total_revenue = parseFloat(fields[6].replace(/\,/g, "")); // Convert to decimal, remove comma
  shift.pending_amount = shift.total_revenue;
  shift.fuel_volume = parseFloat(fields[7].replace(/\,/g, "")); // Convert to decimal
  shift.unit_price = parseFloat(fields[8].replace(/\,/g, "")); // Convert to decimal
  shift.ending_volume = parseInt(fields[9], 10);
  shift.starting_volume = shift.ending_volume - shift.fuel_volume;
  shift.record_id = fields[4] + fields[5];

  shift.fuel_type = fields[1];
  shift.cash = parseFloat(fields[10].replace(/\,/g, "")); // Convert to decimal, remove comma
  shift.card = parseFloat(fields[11].replace(/\,/g, "")); // Convert to decimal, remove comma
  shift.debt = parseFloat(fields[12].replace(/\,/g, "")); // Convert to decimal, remove comma

  data.shift = shift;
}
