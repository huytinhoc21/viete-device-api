import sequelize from "../libs/database";
import { GasTankDetail } from "../types/gas-tank";
import {
  getFields,
  parseHexToFloat32,
  parseTimeDate,
} from "../utils/parse-helper";

export const handleTankStatusData = async (data: {
  tankStatuses: GasTankDetail[];
}) => {
  for (const gt of data.tankStatuses) {
    if (gt.id) {
      await sequelize.query(
        "update gas_tanks set status_data = $1 where id = $2",
        {
          bind: [JSON.stringify(gt.status_data), gt.id],
        }
      );
    }
  }
};

// Function to parse Tank Status frame data
export function parseTankStatusFrame(arrayBuffer: number[], data: any) {
  // Extract frame data

  data.tankStatuses = [];

  // Parse station status data
  let i = 41;
  for (; i < arrayBuffer.length; i += 43) {
    const status = arrayBuffer[i];
    const tankStatusArrayBuffer = arrayBuffer.slice(i + 1, i + 43);
    const tankStatusString = tankStatusArrayBuffer
      .map((byte) => String.fromCharCode(byte))
      .join("");
    if (tankStatusString.length < 42) {
      break;
    }

    const fields = getFields(tankStatusString, [2, 8, 8, 8, 8, 8]);
    if (fields.length < 1) {
      break;
    }

    const tankStatus = {
      status,
      tank_name: fields[0],
      fuel: {
        h: parseHexToFloat32(fields[1]) || 0,
        v: parseHexToFloat32(fields[2]) || 0,
      },
      water: {
        h: parseHexToFloat32(fields[3]) || 0,
        v: parseHexToFloat32(fields[4]) || 0,
      },

      temperature: parseHexToFloat32(fields[5]) || 0,
    };
    data.tankStatuses.push(tankStatus);
  }

  // Parse timestamp
  data.date = parseTimeDate(
    arrayBuffer
      .slice(i, i + 12)
      .map((byte) => String.fromCharCode(byte))
      .join("")
  );

  data.tankStatuses.forEach((_, index) => {
    data.tankStatuses[index].station_time = data.date;
    data.tankStatuses[index].last_connection = new Date();
  });
}
