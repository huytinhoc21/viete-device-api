import { format } from "date-fns";
import { MAX_NUMBER_OF_SAVED_TRANSACTION_IN_PUMP } from "../config";
import memoryCache from "../libs/caching";
import sequelize from "../libs/database";
import { GasStationDetail } from "../types/gas-station";
import { Transaction } from "../types/transaction";
import { apiPost } from "../utils/api-request";
import { getFields, parseDateTime } from "../utils/parse-helper";

export const handleTransactionData = async (
  data: {
    frameType: string;
    stationName: string;
    securityKey: string;
    transaction: { pump_name: string; transaction_index: number } & Pick<
      Transaction,
      | "unit_price"
      | "fuel_volume"
      | "total_revenue"
      | "gas_type"
      | "record_id"
      | "created_at"
    >;
  },
  gasStation: GasStationDetail,
  onReply?: (data: Uint8Array) => Promise<void>
): Promise<any> => {
  const gasPump = gasStation.gas_pumps.find(
    (gp) => gp.name == data.transaction.pump_name
  );
  if (!gasPump) {
    throw new Error("Gas pump not found!");
  }

  const response = await apiPost("/transactions", {
    ...data.transaction,
    gas_pump_id: gasPump.id,
  });

  if (data.frameType == "%CHIT") {
    setTimeout(async () => {
      try {
        const [lastTransactions, _] = await sequelize.query(
          `select * from transactions 
          where deleted_at is null and gas_pump_id = $1 and created_at < $2 order by created_at desc limit 1`,
          { bind: [gasPump.id, data.transaction.created_at] }
        );
        if (lastTransactions[0]) {
          const lastTransaction = lastTransactions[0] as Transaction;
          const { currIndex, lastIndex, prevIndex, numberOfLostTransaction } =
            getTransactionIndex(data.transaction, lastTransaction);

          // if @numberOfLostTransaction >= 21 then we should check manually
          if (lastIndex != prevIndex && numberOfLostTransaction < 21) {
            const todayCount =
              (await memoryCache.get<number>(
                `todayCount-${gasStation.id}-${format(new Date(), "yyyyMMdd")}`
              )) || 0;
            if (todayCount < 213) {
              await memoryCache.set(
                `todayCount-${gasStation.id}-${format(new Date(), "yyyyMMdd")}`,
                todayCount + 1
              );
              const transactionReply = `\r\n${data.stationName}${
                data.securityKey
              }RES${gasPump.name}${(
                (lastIndex + 1) %
                MAX_NUMBER_OF_SAVED_TRANSACTION_IN_PUMP
              )
                .toString()
                .padStart(4, "0")}${prevIndex.toString().padStart(4, "0")}\r\n`;
              await onReply?.(new TextEncoder().encode(transactionReply));
            }
          }
        }
      } catch {}
    }, 1000 * 60 * 21); // 21 minutes
  }

  return { response };
  const lastTransactionIndex: number =
    (await memoryCache.get<number>(
      `transactionIndex-${gasStation.id}-${data.transaction.pump_name}`
    )) || 99999;
  await memoryCache.set(
    `transactionIndex-${gasStation.id}-${data.transaction.pump_name}`,
    data.transaction.transaction_index
  );

  if (lastTransactionIndex < data.transaction.transaction_index - 1) {
    // Reply to request resend data
    const reply = new TextEncoder().encode(
      `\r\n${data.stationName}${data.securityKey}RES${
        data.transaction.pump_name
      }${lastTransactionIndex.toString().padStart(4, "0")}${(
        data.transaction.transaction_index - 1
      )
        .toString()
        .padStart(4, "0")}\r\n`
    );
    console.log("reply", reply);
    sequelize.query(
      "update transactions set device_result = $1 where id = $2",
      { bind: [JSON.stringify(Object.values(reply)), response] }
    );
    return {
      reply,
    };
  }
};
export const resendTransaction = async (
  gasPump: { id: string; name: string },
  data: {
    stationName: string;
    securityKey: string;
    from: string;
    to: string;
  },
  onReply?: (data: Uint8Array) => Promise<void>
) => {
  const MAX_NUMBER_OF_SAVED_TRANSACTION_IN_PUMP = 10000;
  // Resend missed transactions
  const [transactions, __] = await sequelize.query(
    `select * from transactions 
    where deleted_at is null 
      and record_id not like 'cheat%'
      and gas_pump_id = $1
      and created_at < $2
      and created_at >= $3
    order by created_at desc
    limit 1000`,
    {
      bind: [gasPump.id, data.from, data.to],
    }
  );
  const encoder = new TextEncoder();
  let resendCount = 0;
  for (let i = transactions.length - 2; i >= 0; i--) {
    const t = transactions[i] as Transaction;
    const lastTransaction = transactions[i + 1] as Transaction;
    const { currIndex, lastIndex, prevIndex, numberOfLostTransaction } =
      getTransactionIndex(t, lastTransaction);

    // if @numberOfLostTransaction >= 21 then we should check manually
    if (
      lastIndex != prevIndex &&
      numberOfLostTransaction < 21 &&
      resendCount < 2
    ) {
      resendCount++;
      const transactionReply = `\r\n${data.stationName}${data.securityKey}RES${
        gasPump.name
      }${((lastIndex + 1) % MAX_NUMBER_OF_SAVED_TRANSACTION_IN_PUMP)
        .toString()
        .padStart(4, "0")}${prevIndex.toString().padStart(4, "0")}\r\n`;
      await onReply?.(encoder.encode(transactionReply));
      await new Promise((resolve) => setTimeout(() => resolve(""), 1000));
    }
  }
};

const getTransactionIndex = (
  transaction: { record_id: string },
  lastTransaction: { record_id: string }
): {
  lastIndex: number;
  prevIndex: number;
  currIndex: number;
  numberOfLostTransaction: number;
} => {
  const currIndex = parseInt(
    transaction.record_id.substring(transaction.record_id.length - 4)
  );
  const lastRecordId = lastTransaction.record_id;
  const lastIndex = parseInt(lastRecordId.substring(lastRecordId.length - 4));
  const prevIndex =
    (currIndex - 1 + MAX_NUMBER_OF_SAVED_TRANSACTION_IN_PUMP) %
    MAX_NUMBER_OF_SAVED_TRANSACTION_IN_PUMP;
  const numberOfLostTransaction =
    (prevIndex - lastIndex + MAX_NUMBER_OF_SAVED_TRANSACTION_IN_PUMP) %
    MAX_NUMBER_OF_SAVED_TRANSACTION_IN_PUMP;
  return { lastIndex, prevIndex, currIndex, numberOfLostTransaction };
};

// Function to parse Transaction frame data (CHIT, CHWR)
export function parseTransactionFrame(frameData: string, data: any) {
  // Extract frame data
  const frameDataString = frameData.substring(41);

  const fields = getFields(
    frameDataString,
    [2, 3, 6, 6, 13, 11, 6, 12, 1, 10, 4, 4, 5, 1]
  );

  const transaction: any = {};

  transaction.pump_name = fields[0];
  transaction.transaction_index = parseFloat(fields[11]);

  transaction.total_revenue = parseFloat(fields[4].replace(/\,/g, "")); // Convert to decimal, remove comma
  transaction.fuel_volume = parseFloat(fields[5].replace(/\,/g, "")); // Convert to decimal
  transaction.unit_price = parseFloat(fields[6].replace(/\,/g, "")); // Convert to decimal
  transaction.gas_type = fields[1];
  transaction.record_id = fields[2] + fields[11];
  transaction.created_at = new Date(
    parseDateTime(fields[2] + fields[3]).getTime()
  ).toISOString();

  // transaction.identifier = fields[7];
  // transaction.data_type = fields[8];
  // transaction.pump_model = fields[9];
  // transaction.pump_serial = fields[10];
  // transaction.employee_code = fields[12];
  // transaction.shift_code = fields[13]; // Assuming Vi_code is included in shiftCode

  data.transaction = transaction;
}
