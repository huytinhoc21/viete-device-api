import auth from "./auth";
import { parseTankStatusFrame } from "./handlers/tank-status";
import sequelize from "./libs/database";
import startStationServer from "./server";
import test from "./test";
import { GasPumpDetail } from "./types/gas-pump";
import { GasStationDetail } from "./types/gas-station";
import { GasTank } from "./types/gas-tank";
import { retry } from "./utils/api-request";

const startServer = async () => {
  const [stations, _] = await sequelize.query(
    `select * from gas_stations 
    where deleted_at is null and device_port is not null and device_port > 0 and ws_port > 0`
  );
  const [gasPumps, __] = await sequelize.query(
    "select * from gas_pumps where deleted_at is null"
  );
  const [gasTanks, ___] = await sequelize.query(
    "select * from gas_tanks where deleted_at is null"
  );
  const gasStations: GasStationDetail[] = stations.map(
    (station: GasStationDetail) => {
      const gas_tanks = gasTanks.filter(
        (gt: GasTank) => gt.gas_station_id == station.id
      ) as GasTank[];

      const gas_pumps = gasPumps.filter((gp: GasPumpDetail) =>
        gas_tanks.some((gt) => gp.gas_tank_id == gt.id)
      ) as GasPumpDetail[];
      return {
        ...station,
        gas_tanks: gas_tanks.map((gt) => ({
          ...gt,
          gas_pumps: gas_pumps.filter((gp) => gp.gas_tank_id == gt.id),
        })),
        gas_pumps,
      };
    }
  );

  gasStations.forEach((gs) => {
    console.log("gs", gs);
    console.log("gs.gas_tanks", gs.gas_tanks);
    console.log("gs.gas_pumps", gs.gas_pumps);
  });
  console.log("gasStations", gasStations);

  gasStations
    .filter((station) => !!station.device_port)
    .forEach((station) => {
      startStationServer(station);
    });

  // const testGasStation = gasStations.find(
  //   (gs) => Number(gs.device_port) == 8081
  // );
  // if (testGasStation) {
  //   test(testGasStation);
  // }

  console.log("stations", stations);
};

const main = async () => {
  await sequelize.authenticate();
  await retry(auth);
  await retry(startServer);
};

const t = async () => {
  const data: any = {};
  parseTankStatusFrame(
    [
      37, 84, 65, 78, 75, 67, 72, 88, 68, 66, 73, 78, 72, 80, 72, 85, 79, 67,
      48, 57, 48, 67, 85, 65, 72, 65, 78, 71, 88, 68, 66, 73, 78, 72, 80, 72,
      85, 79, 67, 57, 48, 7, 48, 49, 52, 53, 48, 53, 51, 66, 68, 66, 52, 54, 49,
      48, 68, 54, 55, 54, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48,
      48, 48, 48, 52, 50, 48, 49, 48, 67, 67, 67, 5, 48, 50, 63, 63, 63, 63, 63,
      63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63,
      63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 5, 48,
      51, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63,
      63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63,
      63, 63, 63, 63, 63, 5, 48, 52, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63,
      63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63,
      63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 12,
    ],
    data
  );
  console.log("data", data.tankStatuses);
};

// t();

main();
