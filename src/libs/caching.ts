import { createCache, memoryStore } from "cache-manager";

const memoryCache = createCache(memoryStore());

export default memoryCache;
