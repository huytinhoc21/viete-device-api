import { Sequelize } from "sequelize";

const sequelize = new Sequelize(
  process.env.DB_NAME || "petro-station",
  process.env.DB_USERNAME || "admin",
  process.env.DB_PASSWORD || "123",
  {
    host: process.env.DB_HOST,
    dialect: "postgres",
    port: Number(process.env.DB_PORT || "5432"),
    logging: () => {},
  }
);

export default sequelize;
