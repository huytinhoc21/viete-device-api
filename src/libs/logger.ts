import { QueryTypes } from "sequelize";
import { GasStationDetail } from "../types/gas-station";
import sequelize from "../libs/database";

export const logResendData = async (
  arrayBuffer: number[],
  gasStation: GasStationDetail
): Promise<string> => {
  const [results, _] = await sequelize.query(
    "insert into device_data (gas_station_id, data, type, created_at) values ($1, $2, 'resend', now()) returning id",
    {
      bind: [gasStation.id, JSON.stringify(arrayBuffer)],
      type: QueryTypes.INSERT,
    }
  );
  return results?.[0]?.id || "";
};

export const logDeviceData = async (
  arrayBuffer: number[],
  gasStation: GasStationDetail
): Promise<string> => {
  const [results, _] = await sequelize.query(
    "insert into device_data (gas_station_id, data, type, created_at) values ($1, $2, 'raw-data', now()) returning id",
    {
      bind: [gasStation.id, JSON.stringify(arrayBuffer)],
      type: QueryTypes.INSERT,
    }
  );
  return results?.[0]?.id || "";
};

export const logDeviceStatus = (
  status: "ok" | "ended" | "closed" | "error",
  gasStation: GasStationDetail
) => {
  const timestamp = new Date().getTime();
  sequelize.query("update gas_stations set device_status = $1 where id = $2", {
    bind: [`${status}-${timestamp}`, gasStation.id],
  });
};

export const logHandleDataError = async (
  err: any,
  gasStation: GasStationDetail
) => {
  const [results, _] = await sequelize.query(
    "insert into device_data (gas_station_id, error, type, created_at) values ($1, $2, 'handle-error', now()) returning id",
    {
      bind: [gasStation.id, JSON.stringify(err)],
      type: QueryTypes.INSERT,
    }
  );
  return results?.[0]?.id || "";
};
