import net from "net";
import handleFrameData, { checkMissedTransactions } from "./handlers";
import { GasStationDetail } from "./types/gas-station";

import WebSocket, { WebSocketServer } from "ws";
import chalk from "chalk";
import {
  logDeviceData,
  logDeviceStatus,
  logHandleDataError,
  logResendData,
} from "./libs/logger";
import memoryCache from "./libs/caching";

const log = (message: string, tcpPort: number) => {
  console.log(chalk.bgBlue(`[${tcpPort}]`) + " " + message);
};

const startStationServer = (gasStation: GasStationDetail) => {
  if (!gasStation.device_port || !gasStation.ws_port) {
    return;
  }
  const tcpPort = gasStation.device_port;
  const wsPort = gasStation.ws_port;
  // Create a WebSocket server
  const wsServer = new WebSocketServer({ port: wsPort });

  // Store connected TCP sockets
  const tcpSockets = new Set<net.Socket>();

  // Create a new TCP server
  const tcpServer = net.createServer((tcpSocket) => {
    log(chalk.yellow("TCP client connected"), tcpPort);
    tcpSockets.add(tcpSocket);

    // test
    setTimeout(async () => {
      console.log("checkMissedTransactions");
      while (1) {
        const stationName = (await memoryCache.get(
          "stationName_" + gasStation.id
        )) as string;
        const securityKey = (await memoryCache.get(
          "securityKey_" + gasStation.id
        )) as string;
        if (stationName && securityKey) {
          break;
        }
        await new Promise((resolve) => setTimeout(() => resolve(""), 10000));
      }
      await checkMissedTransactions(gasStation, async (data) => {
        tcpSocket.write(data, (err) => {
          log(chalk.green(`Send to TCP client: ${data.toString()}`), tcpPort);
          logResendData(Array.from(data), gasStation);
          if (err) {
            log(chalk.red(`Reply error: ${err}`), tcpPort);
          }
        });
      });
    }, 30000);

    const interval = setInterval(async () => {
      try {
        await checkMissedTransactions(gasStation, async (data) => {
          tcpSocket.write(data, (err) => {
            log(chalk.green(`Send to TCP client: ${data.toString()}`), tcpPort);
            logResendData(Array.from(data), gasStation);
            if (err) {
              log(chalk.red(`Reply error: ${err}`), tcpPort);
            }
          });
        });
      } catch (err) {
        logHandleDataError(err, gasStation);
      }
    }, 1000 * 60 * 60);

    // Handle data received from the TCP client
    tcpSocket.on("data", async (data) => {
      try {
        log(
          chalk.green(`Received from TCP client: ${data.toString()}`),
          tcpPort
        );

        if (data.toString().startsWith("%")) {
          const { parsedData, result } = await handleFrameData(
            data,
            gasStation,
            async (data) => {
              tcpSocket.write(data, (err) => {
                log(
                  chalk.green(`Send to TCP client: ${data.toString()}`),
                  tcpPort
                );
                logResendData(Array.from(data), gasStation);
                if (err) {
                  log(chalk.red(`Reply error: ${err}`), tcpPort);
                }
              });
            }
          );
          if (parsedData.frameType.toString().startsWith("%CONN")) {
            logDeviceStatus("ok", gasStation);
          }

          // Send the data to all connected WebSocket clients
          wsServer.clients.forEach((ws) => {
            if (ws.readyState === WebSocket.OPEN) {
              ws.send(
                JSON.stringify({
                  parsed_data: parsedData,
                  result,
                  data: Array.from(data),
                })
              );
            }
          });
        } else if (data.toString().startsWith("@")) {
          wsServer.clients.forEach((ws) => {
            if (ws.readyState === WebSocket.OPEN) {
              ws.send(
                JSON.stringify({
                  data: Array.from(data),
                })
              );
            }
          });
          const arrayBuffer = Array.from(data);
          logDeviceData(arrayBuffer, gasStation);
        } else {
          const arrayBuffer = Array.from(data);
          logDeviceData(arrayBuffer, gasStation);
        }
      } catch (err) {
        log(chalk.red(`Error processing TCP data: ${err}`), tcpPort);
        logHandleDataError(err, gasStation);
      }
    });

    // Handle TCP client disconnection
    tcpSocket.on("end", () => {
      log(chalk.yellow("TCP client disconnected"), tcpPort);
      logDeviceStatus("ended", gasStation);
      tcpSockets.delete(tcpSocket);
      clearInterval(interval);
    });

    tcpSocket.on("close", () => {
      log(chalk.yellow("TCP client closed"), tcpPort);
      logDeviceStatus("closed", gasStation);
      tcpSockets.delete(tcpSocket);
      clearInterval(interval);
    });

    tcpSocket.on("error", (err) => {
      log(chalk.yellow("TCP client closed: " + err), tcpPort);
      logDeviceStatus("error", gasStation);
      tcpSockets.delete(tcpSocket);
      clearInterval(interval);
    });
  });

  wsServer.on("connection", (ws) => {
    log(chalk.yellow("WS client connected"), wsPort);

    ws.on("message", (message) => {
      log(
        chalk.green(`Received message from WS client: ${message.toString()}`),
        wsPort
      );

      ws.send(
        JSON.stringify({
          data: Object.values(
            new TextEncoder().encode("Received: " + message.toString())
          ),
          parsed_data: {},
          result: {},
        })
      );

      if (Buffer.isBuffer(message)) {
        // Forward the WebSocket message to the TCP clients
        tcpSockets.forEach((tcpSocket) => {
          tcpSocket.write(new Uint8Array(message), (err) => {
            if (err) {
              log(
                chalk.red(`Error sending message to TCP client: ${err}`),
                wsPort
              );
            }
          });
        });
      }
    });

    ws.on("close", () => {
      log(chalk.yellow("WS client disconnected"), wsPort);
    });
  });

  //Start listening on the specified port
  tcpServer.listen(tcpPort, () => {
    log(
      chalk.green(`Server listening for station ${gasStation.name}`),
      tcpPort
    );
  });

  const shutdown = () => {
    tcpServer.close(() => {
      log(chalk.red("TCP server closed"), tcpPort);
    });
    wsServer.close(() => {
      log(chalk.red("WebSocket server closed"), wsPort);
    });
    tcpSockets.forEach((socket) => socket.destroy());
    process.exit();
  };

  process.on("SIGTERM", shutdown);
  process.on("SIGINT", shutdown);
  process.on("SIGQUIT", shutdown);
};

export default startStationServer;
