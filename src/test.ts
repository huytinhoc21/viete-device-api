import { parsePumpStatus, parsePumpStatusFrame } from "./handlers/pump-status";
import sequelize from "./libs/database";
import { GasStationDetail } from "./types/gas-station";

const test = async (gasStation: GasStationDetail) => {
  const [deviceData, _] = await sequelize.query(
    "select * from device_data order by created_at desc limit 100"
  );
  for (const dd of deviceData) {
    const { data: _data } = dd as any;
    console.log("data", _data);
    const data = Buffer.from(JSON.parse(_data.toString()));
    try {
      if (
        data.toString().startsWith("%CHWR") ||
        data.toString().startsWith("%CHIT")
      ) {
        console.log(`[] Received from database:`, data.toString());

        // const { parsedData, result } = await handleFrameData(data, gasStation);
        const arrayBuffer = Array.from(data);
        const res = {};
        parsePumpStatusFrame(arrayBuffer, res);
        console.log("res", res);

        await new Promise((resolve) => setTimeout(() => resolve(""), 1000));
      } else {
        const arrayBuffer = Array.from(data);
      }
    } catch (err) {
      console.log("err", err);
    }
  }
};

export default test;
