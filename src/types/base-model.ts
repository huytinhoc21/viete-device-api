export interface BaseModel {
  id: string;
  name: string;
}

export const initialBaseMode: BaseModel = {
  id: "",
  name: "",
};
