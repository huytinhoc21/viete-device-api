export interface GasPump {
  gas_tank_id: string;
  id: string;
  name: string;
  status_data: GasPumpStatusData;
  updated_at: string;
}

export interface GasPumpDetail extends GasPump {}

export interface GasPumpStatusData {
  disconnected?: boolean;
  fuel_volume: number;
  status: number | string;
  total_cost: number;
  unit_price: number;
  station_time: string;
  gas_type: string;
}

export const initialGasPump: GasPumpDetail = {
  gas_tank_id: "",
  id: "",
  name: "",
  status_data: {
    fuel_volume: 0,
    status: 0,
    total_cost: 0,
    unit_price: 0,
    station_time: "",
    gas_type: "",
  },
  updated_at: "",
};
