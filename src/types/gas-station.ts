import { GasPump } from "./gas-pump";
import { GasTank } from "./gas-tank";

export interface GasStation {
  id: string;
  name: string;
  station_chief: string;
  address: string;
  num_employees: number;
  phone?: string;
  opening_hours?: Date | string;
  closing_hours?: Date | string;
  allow_transaction_manualy?: boolean;
  device_port?: number;
  ws_port?: number;
}

export interface GasStationDetail extends GasStation {
  gas_pumps: GasPump[];
  gas_tanks: GasTank[];
}
