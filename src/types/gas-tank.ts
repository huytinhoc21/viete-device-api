import { BaseModel } from "./base-model";
import { GasStation } from "./gas-station";

export interface GasTank extends BaseModel {
  gas_station_id: string;
  capacity: number;
  gas_pumps: BaseModel[];
  good_id: number;
  device_name: string;
}

export interface GasTankDetail extends Omit<GasTank, "good_id"> {
  gas_station?: GasStation;
  status_data: GasTankStatusData;
  good?: { id: number; name: string };
}

export interface GasTankStatusData {
  fuel: GasTankStatusDataFuel;
  last_connection: string | Date;
  measuring_stick: number;
  remaining: number;
  status: string;
  temperature: number;
  water: GasTankStatusDataFuel;
}

export interface GasTankStatusDataFuel {
  h: number;
  v: number;
}

export const initialGasTank: GasTank & GasTankDetail = {
  gas_station_id: "",
  capacity: 0,
  gas_pumps: [],
  good_id: 0,
  status_data: {
    fuel: {
      h: 0,
      v: 0,
    },
    remaining: 0,
    last_connection: "",
    measuring_stick: 0,
    status: "",
    temperature: 0,
    water: {
      h: 0,
      v: 0,
    },
  },
  id: "",
  name: "",
  device_name: "",
};
