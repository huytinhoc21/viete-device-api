export interface StaffShift {
  actual_end_time: string;
  actual_start_time: string;
  date: string;
  ending_volume: number;
  fuel_volume: number;
  starting_volume: number;
  total_revenue: number;
  unit_price: number;
  gas_pump_id: string;
  shift_id: string;
  staff_id: string;
  good?: string;
  record_id: string;
}
