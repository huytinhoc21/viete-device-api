export interface Transaction {
  gas_pump_id: string;
  fuel_volume: number;
  total_revenue: number;
  unit_price: number;
  gas_type: string;
  record_id: string;
  created_at: Date;
}
