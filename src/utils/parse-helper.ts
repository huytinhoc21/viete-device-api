// HHmmssddMMyy
export const parseTimeDate = (str: string) => {
  if (!str || str.length !== 12) {
    return null; // Invalid format
  }

  const year = parseInt(str.slice(10, 12), 10) + 2000;
  const month = parseInt(str.slice(8, 10), 10) - 1; // Months are zero-indexed
  const day = parseInt(str.slice(6, 8), 10);
  const hours = parseInt(str.slice(0, 2), 10);
  const minutes = parseInt(str.slice(2, 4), 10);
  const seconds = parseInt(str.slice(4, 6), 10);

  return new Date(year, month, day, hours, minutes, seconds);
};

// ddMMyyHHmmss
export const parseDateTime = (str: string) => {
  if (!str || str.length !== 12) {
    return null; // Invalid format
  }

  const year = parseInt(str.slice(4, 6), 10) + 2000;
  const month = parseInt(str.slice(2, 4), 10) - 1; // Months are zero-indexed
  const day = parseInt(str.slice(0, 2), 10);
  const hours = parseInt(str.slice(6, 8), 10);
  const minutes = parseInt(str.slice(8, 10), 10);
  const seconds = parseInt(str.slice(10, 12), 10);

  return new Date(year, month, day, hours, minutes, seconds);
};

export const getFields = (str: string, fieldLengths: number[]): string[] => {
  const fields: string[] = [];
  let i = 0;

  fieldLengths.forEach((len) => {
    fields.push(str.substring(i, i + len));
    i += len;
  });
  return fields;
};

export function parseHexToFloat32(hexString: string): number {
  // Ensure the input is a valid hex string
  if (!/^[0-9A-Fa-f]{8}$/.test(hexString)) {
    return null;
  }

  // Convert the hex string to a 32-bit integer
  const int = parseInt(hexString, 16);

  // Create a buffer to store the 32-bit integer
  const buffer = new ArrayBuffer(4);
  const intView = new Uint32Array(buffer);
  intView[0] = int;

  // Create a float32 view on the buffer
  const floatView = new Float32Array(buffer);

  // Return the float value
  return floatView[0];
}
