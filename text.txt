write javascript function to parse input frame data with document """1. Frame Connect mạng khi mới powerup:

<%CONN(5 char)><tên trạm(16 char)>< SECURITY KEY (20 char)><0x0C>.

2. Frame Connect lại sau khi bị mất kết nối hoặc gửi data không thành công:

<%RECO(5 char)><tên trạm(16 char)>< SECURITY KEY (20 char)><0x0C> .

3. Frame Trạng thái :

<%STAT(5)> +< tên trạm(16) ><security_key(20)><các trạng thái trụ thứ i ( 25 char/trụ)><0x0C>

- Trong đó, trạng thái trụ thứ i bao gồm:

<Stat(1)><trụ (2)><loại nhiên liệu (3)> +< tiền(6)><lít(6)><giá(6)><coeff(1)>

+ Stat : byte chỉ trạng thái trụ, có giá trị Hex <0x20

<0x05>: FD bị mất kết nối với master

<0x06>: FD đang bơm.

<0x08> :FD không bơm .

+ Hệ số:coeff chứa thông số hệ số nhân 10x cho giá trị tiền (coeff_tien) / lít (coeff_lit ). Sử dụng low nibble trong hệ số coeff để xác định hệ số nhân cho tiền/lít theo cách sau:

coeff_c    = (coeff-0x30) &0x0F; //lấy lại low nibble từ master

coeff_lit   = coeff_c& 0x03;       //2 bit thấp của nibble, hệ số nhân lít

coeff_tien =coeff_c>>2;          //2 bit cao của nibble, hệ số nhân tiền

+ Đơn vị tiền: VNĐ.

+ Đơn vị lít: ml.

Thí dụ:



%STATCHXDBINHPHUOC010CUAHANGXDBINHPHUOC1001000000000000000000000002000000000000000000000003000000000000000000000004DO50310000001210254531152629170117

 

Frame trên gồm:

1-Tên trạm : CHXDBINHPHUOC010

2-Security key : CUAHANGXDBINHPHUOC10

3- Trạng thái trụ: có 4 trụ trong frame

01000000000000000000000002000000000000000000000003000000000000000000000004DO50310000001210254531

- Trụ 1/2/3 : mất kết nối

- Trụ 04:04DO50310000001210254531152629170117

     + Trạng thái trụ: <0x06> , không bơm

     + Tru:04

     + Loại nhiên liệu: DO5

     + tiền : 031000

     + lít: 000121

     + giá: 025453 => giá=25.453 VND

     + coeff:1

Với coeff=1>>Hex= 0x31>>coeff_c=1>>coeff_lit=1,coeff_tien=0

= giá trị tiền = 31.000; giá trị lít=1210ml=1,21 lít;

4-time/date : 152629170117

 + Time:15:26:29

 + Date: 17/01/17

 

4. Frame Data thông thường: gồm frame data giao dịch chi tiết và frame data giao ca

<Mã phân loại data(5 char)><tên trạm(16 char)><SECURITY KEY(20 char) ><datagiao dịch(78)><0x0C> = <100 byte>

<Mã phân loại data(5 char)><tên trạm(16 char)><SECURITY KEY(20 char) ><data giao ca(104)><0x0C> =<126byte>

* Mã phân loại data(5 char) chỉ các kiểu data sau:

     - Data chi tiết giao dịch lần bơm bình thường: %CHIT

%CHITCHXDBINHPHUOC010CUAHANGXDBINHPHUOC1004DO5170117155213 50,000 1.96025,453????????????0SPR2017-SE10040374000020

     - Data giao ca bình thường: %GICA

%GICACHXDBINHPHUOC010CUAHANGXDBINHPHUOC1004 DO5020517101606 030517110211 6,273,830 236.31025,453171036 0 0 6,273,830

 

     - Data chi tiết giao dịch lần bơm được lưu trong bộ nhớ của master trong tình huống master mất kết nối với mạng hoặc data gửi theo manual mode: %CHWR

     - Data giao ca lưu trong bộ nhớ của master master trong tình huống modem mất kết nối với mạng hoặc data gửi theo manual mode: %GIWR

* Data chi tiết giao dịch:

<tru(02) >< loại nhien liệu(03) ><ngày(06)> +< giờ (06)><tiền (13)>< lít(11)><giá (06)> +<soxe/ID_card/bank_code/QR_code(12)><data_type(1)><model_trụ(10 char)><serial_trụ (4char) >< index_chitiet(4 char)><NV_code(05)> +< Vi_code(1) >=<84 byte>

04DO5170117155213 50,000 1.96025,453????????????0SPR2017-SE10040374000020

GHI CHÚ: index_chitiet(4 char)   : là số thứ tự lần giao dịch của FD, dùng làm tham số đối soát , reload khi có sự cố mất data.

GHI CHÚ: data_type(1 char): chỉ loại data của bản tin, qui ước như sau

    -data_type =0: CASH, 12 byte liền trước Không quan tâm.

    -data_type =1: SALE, 12 byte liền trước là bank_code.

   -data_type =2: REPAY, 12 byte liền trước là bank_code.

   -data_type =3: VOID, 12 byte liền trước là bank_code.

    -data_type =4: GHINO, 12 byte liền trước là Số Xe .

   -data_type =5: KHUYEN MAI, 12 byte liền trước là Số xe.

   -data_type= 6: CHUYENKHOAN_QR/TRATRUOC_QR, 12 byte liền trước là ID_QR .

   -data_type =7: KHUYENMAI_QR, 12 byte liền trước là ID_R .

   -data_type =8: CHUYENKHOAN_CARD/TRATRUOC_CARD, 12 byte liền trước là ID_KHACHHANG .

   -data_type= 9: GHINO_QR, 12 byte liền trước là Số xe.

   -data_type =A,B,C,……..Dành để mở rộng cho các kiểu giao dịch KHÁC,….

GHI CHÚ: Model (10 char): là model của FD (t.dụ :VPE2014-TO hoac SPR2017-AT)

GHI CHÚ: Serial(4 digit): số seri xuất xưởng của FD

GHI CHÚ: Vi_code (1 char): là code kết hợp với data_type(1 char) để phân biết các loại thanh toán ( tiền mặt/chuyển khoản: loại ví, loại thẻ,….)

 

* Data giao ca:

<tru(02)><loại(03)><ngày đầu ca(06)><giờ đầu ca(06)><ngày cuối ca(06)>< giờ cuối ca(06)><tiền_TỔNG (13)>< lít(11)><giá (06)> +< total (06)> +< tien_CK(13)><tien_GHINO(13)><tien_MAT(13) >=<104 byte>

04DO5020517101606 030517110211 6,273,830 236.31025,453171036 0 0 6,273,830

GHI CHÚ: trong đó data chỉ loại data TIỀN gồm

   -tiền_TỔNG: tổng tiền giao dịch qua FD = tiền CASH+tiền CK + tiền_GHINO ( Thẻ nội bộ hoặc QR).

   - tiền_MẶT: tổng tiền mặt thu trong ca.

   -tiền_CK: tổng tiền giao dịch thanh toán không tiền mặt

   - tiền_GHINO: tổng tiền giao dịch qua thẻ GHI NỢ nội bộ hoặc qua ứng dụng QR Code của chuỗi.

* <0x0C>: ký tự điều khiển Form feed (0x0C Hex)"""